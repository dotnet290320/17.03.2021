using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace Wpf170321
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class Person
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public override string ToString()
            {
                return $"{FirstName} {LastName}";
            }
        }

        private List<Person> people = new List<Person>();
        public MainWindow()
        {
            InitializeComponent();

            people.Add(new Person { FirstName = "Dan", LastName = "Cohen" });
            people.Add(new Person { FirstName = "Eli", LastName = "Snir" });
            people.Add(new Person { FirstName = "Mariano", LastName = "Boser" });
            people.Add(new Person { FirstName = "Suzana", LastName = "Holmes" });

            myComboBox.ItemsSource = people;
            myComboBox.SelectedIndex = 0;
        }

        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Xceed.Wpf.Toolkit.MessageBox.Show("hi");
        }

        private void submitBtn_Click(object sender, RoutedEventArgs e)
        {
            //Xceed.Wpf.Toolkit.MessageBox.Show("hi");
            people.Add(new Person { FirstName = firstNameTxt.Text, LastName = lastNameTxt.Text });
        }
        // remove button1
        // when clicked then remove the person with matching first name and last name
        // remove button2
        // when clicked then remove the selected person
        // create TextBlock which the text inside wit will be the combo selected item

        // DELETE:
        // myComboBox.ItemsSource = null;
        // myComboBox.Items.Clear();
        // people.RemoveAt(0);
        // myComboBox.ItemsSource = people;
    }
}
